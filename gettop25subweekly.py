#Copyright: myshar.org
import datetime
import praw
#CONFIG:
NUMBER_OF_THREAD_TO_PARSE = 3

def parse_reddit():
	r = praw.Reddit(user_agent="Best Of Reddit 1.0 - myshar.org")
	subs = []
	with open("subs.txt", "r") as textfile:
		for each in textfile:
			each = each[:-1]
			subs.append(each)
	for each in subs:			
		sub_content = r.get_subreddit(each)
		array = []
		array.append(str(each))
		for post in (sub_content.get_top_from_week(limit=NUMBER_OF_THREAD_TO_PARSE)):#default=25
			array.append(post.url)
		data.append(array)

def check_txt_file():
	textfile = open("Last_Time.txt", "r+")
	try:
		time = int(textfile.readline()) + 1
		textfile.close()

		textfile = open("Last_Time.txt", "r+")
		textfile.truncate()
		textfile.write(str(time))
		textfile.close()
	except Exception:
		raise Exception("ERROR - CHECK Last_Time.txt")

	return time

def create_txt_file(data):
	with open (title, "w+") as textfile:
		insert_first_line(time_count)
		lines = insert_template()
		for each in lines:
			textfile.write(each)
		for each in data:
			for items in each:
				if (items[0] != "h"):
				 	textfile.write(" \n \n \n")
				 	textfile.write("**" + items[0].upper() + items[1:] + "**" + " \n \n")#uppercase first letter
				else:
					textfile.write("- [](" + items + ")" + " \n")

def insert_first_line(time_count):
	with open ("Template.txt", "r") as textfile:
		first_line = textfile.readline()
		first_line = first_line.replace("x", str(time_count))
	return first_line

def insert_template():
	with open ("Template.txt", "r") as textfile:
		lines = textfile.readlines()[1:]
	return lines
def main():
	if (datetime.datetime.today().weekday()) == 4:
		data = []
		print ("Parsing data from reddit, it'll take a few minutes...")
		parse_reddit()
		print ("Creating txt file...")
		time_count = check_txt_file()
		print ("Writing txt file...")
		title = "Best of Reddit - Issue" + str(time_count) + ".txt"
		create_txt_file(data)
		print ("Done!")
	else:
		print ("It's not Friday.")
if __name__ == '__main__':
	main()
